//
//  MyCollectionViewCell.swift
//  PVInView
//
//  Created by user176749 on 10/10/20.
//  Copyright © 2020 YOUNGSIC KIM. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var DataView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!

    
    static let identifier="MyCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
 //       self.widthConstraint.constant = UIScreen.main.bounds.width

    }

    
    
    static func nib() ->UINib {
        return UINib(nibName: "MyCollectionViewCell",bundle: nil)
    }
    
    
    
}
