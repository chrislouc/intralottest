

import UIKit

class VideoVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var videoTableView: UITableView!
    
     var data :[DataResponse] = []
    override func viewDidLoad() {
        super.viewDidLoad()

         videoTableView.register(MyTableViewCell.nib(), forCellReuseIdentifier: MyTableViewCell.id)
          videoTableView.delegate = self
        videoTableView.dataSource = self
        
        ApiHelper.getData(type:2) { result in
            switch result {
                
            case .failure(let error):
                        print(error)

        case .success(let value):
            guard let data=value.data else {return }

            self.data=data
           //print( self.data[0].attributes?.card_artwork_url)
            }
              self.data = self.data.sorted(by: { ($0.attributes?.released_at ?? "") > ($1.attributes?.released_at ?? "" )})
            self.videoTableView.reloadData()
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MyTableViewCell = self.videoTableView.dequeueReusableCell(withIdentifier: MyTableViewCell.id) as! MyTableViewCell
        
        let currentVideo = self.data[indexPath.row]
        cell.descriptionLabel.text=currentVideo.attributes?.description
           // cell.myCellLabel.text = self.animals[indexPath.row]
        
        cell.artworkImageView.sd_setImage(with: URL(string: (currentVideo.attributes?.card_artwork_url)!), placeholderImage: nil)
      
       
        
     
        cell.releaseDateLabel.text!=ApiHelper.convertDate(apiDate:currentVideo.attributes?.released_at)
        cell.nameLabel.text=currentVideo.attributes?.name
        cell.typeLabel.text="Video"
        return cell
    }
    
}
